<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $row has been initialized with the current post

echo '<h2>Comentarios</h2>';

// Show the comments of the current post
echo '<ul>';
foreach ($row['comments'] as $comment) {
	echo '<li>' . $comment['author'] . ': ' . substr($comment['body'], 0, 100) . ' ... </li> ';
}
echo '</ul>';
?>