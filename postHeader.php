<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $row has been initialized with the current post

	echo '<div class="blog-post">';

	// Show post title
	echo '<h2 class="blog-post-title">' . $row['title'] . '</h2>';

	// Show post date and author
	echo '<p class="blog-post-meta">' . $row['date']->toDateTime()->format('l, d F Y' ) . ' by <a href="#">' . $row['author'] . '</a></p>';
?>