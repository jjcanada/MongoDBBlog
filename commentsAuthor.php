<?php
// Connect to the database 
include_once("connection.php");

// Store in an array the (distinct) authors stored in the collection
$authorArray = $collection->distinct('comments.author');

echo '<h4>Autores de comentarios</h4>';
echo '<ol class="list-unstyled">';

// Show the authors
// Each author is an hyperlink to index.php with this two GET parameters:
// command: 'showPostsCommentedByAuthor' 
// name: the name of the author
foreach ($authorArray as $author) {
	echo '<li><a href = "index.php?command=showPostsCommentedByAuthor&author=' . 
		$author . '">' . $author . '</a></li>';
}
echo '</ol>';
?>